import { Grid } from "@mui/material";
import Navbar from "../components/Navs/Navbar";
import Box from "@mui/material/Box";
import LeftBar from "../components/Navs/LeftBar";
import Feed from "./../components/Main/Feed";
import RightBar from "../components/RightBar/RightBar";
import AddPostModal from "../components/Main/AddPostModal";

function App() {
    return (
        <>
            <Navbar />
            <Box sx={{ marginTop: "4rem" }}>
                <Grid container>
                    <Grid item xs="2" sm="2">
                        <LeftBar />
                    </Grid>
                    <Grid item xs="9" sm="7">
                        <Feed />
                    </Grid>
                    <Grid
                        item
                        sm="3"
                        sx={{ display: { xs: "none", md: "block" } }}>
                        <RightBar />
                    </Grid>
                </Grid>
            </Box>
            <AddPostModal />
        </>
    );
}

export default App;
