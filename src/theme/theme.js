import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primary: {
      main: "#525B6D",
    },
    secondary: {
      main: "#746F80",
    },
  },
});

export default theme;