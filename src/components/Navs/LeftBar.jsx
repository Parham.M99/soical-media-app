import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import { Typography } from "@mui/material";
import HomeIcon from "@mui/icons-material/Home";
import { styled, alpha } from "@mui/material/styles";
import PersonIcon from "@mui/icons-material/Person";
import FormatListBulletedIcon from "@mui/icons-material/FormatListBulleted";
import CameraIcon from "@mui/icons-material/Camera";
import SlowMotionVideoIcon from "@mui/icons-material/SlowMotionVideo";
import TabletMacIcon from "@mui/icons-material/TabletMac";
import BookmarkIcon from "@mui/icons-material/Bookmark";
import StorefrontIcon from "@mui/icons-material/Storefront";
import SettingsIcon from "@mui/icons-material/Settings";
import Container from "@mui/material/Container";

const BoxContainer = styled(Container)(({ theme }) => ({
    backgroundColor: theme.palette.common.white,
    position: "sticky",
    top: 50,
    height: "100vh",
    borderRight: "1px solid lightgrey",
    paddingTop: theme.spacing(2),
    [theme.breakpoints.down("sm")]: {
        display: "flex",
        justifyContent: "center",
    },
    [theme.breakpoints.between("md", "lg")]: {
        paddingLeft: "0",
    },
}));

const WrapperItem = styled(Box)(({ theme }) => ({
    marginLeft: theme.spacing(1),
    [theme.breakpoints.down("md")]: {
        ".MuiButtonBase-root": {
            paddingLeft: theme.spacing(0.5),
            paddingRight: theme.spacing(0.5),
            paddingTop: theme.spacing(0.5),
        },
        marginLeft: 0,
    },
}));

const BoxItem = styled("div")(({ theme }) => ({
    transition: "all .7s",
    marginBottom: ".7rem",
    cursor: "pointer",
    "&:hover": {
        transform: "scale(1.1)",
        ".MuiButtonBase-root ": {
            color: theme.palette.secondary.main,
        },
    },
}));

const TextItem = styled(Typography)(({ theme }) => ({
    [theme.breakpoints.down("md")]: {
        display: "none",
    },
}));

function LeftBar() {
    return (
        <BoxContainer>
            <WrapperItem>
                <BoxItem>
                    <IconButton size="large">
                        <HomeIcon
                            sx={{ fontSize: { xs: "1.8rem", md: "2rem" } }}
                        />
                        <TextItem variant="h6" component="span">
                            Homepage
                        </TextItem>
                    </IconButton>
                </BoxItem>
                <BoxItem>
                    <IconButton size="large">
                        <PersonIcon
                            sx={{ fontSize: { xs: "1.8rem", md: "2rem" } }}
                        />
                        <TextItem variant="h6" component="span">
                            Friends
                        </TextItem>
                    </IconButton>
                </BoxItem>
                <BoxItem>
                    <IconButton size="large">
                        <FormatListBulletedIcon
                            sx={{ fontSize: { xs: "1.8rem", md: "2rem" } }}
                        />
                        <TextItem variant="h6" component="span">
                            Lists
                        </TextItem>
                    </IconButton>
                </BoxItem>
                <BoxItem>
                    <IconButton size="large">
                        <CameraIcon
                            sx={{ fontSize: { xs: "1.8rem", md: "2rem" } }}
                        />
                        <TextItem variant="h6" component="span">
                            Camera
                        </TextItem>
                    </IconButton>
                </BoxItem>
                <BoxItem>
                    <IconButton size="large">
                        <SlowMotionVideoIcon
                            sx={{ fontSize: { xs: "1.8rem", md: "2rem" } }}
                        />
                        <TextItem variant="h6" component="span">
                            Videos
                        </TextItem>
                    </IconButton>
                </BoxItem>
                <BoxItem>
                    <IconButton size="large">
                        <TabletMacIcon
                            sx={{ fontSize: { xs: "1.8rem", md: "2rem" } }}
                        />
                        <TextItem variant="h6" component="span">
                            Apps
                        </TextItem>
                    </IconButton>
                </BoxItem>
                <BoxItem>
                    <IconButton size="large">
                        <StorefrontIcon
                            sx={{ fontSize: { xs: "1.8rem", md: "2rem" } }}
                        />
                        <TextItem variant="h6" component="span">
                            Market Place
                        </TextItem>
                    </IconButton>
                </BoxItem>
                <BoxItem>
                    <IconButton size="large">
                        <SettingsIcon
                            sx={{ fontSize: { xs: "1.8rem", md: "2rem" } }}
                        />
                        <TextItem variant="h6" component="span">
                            Settings
                        </TextItem>
                    </IconButton>
                </BoxItem>
                <BoxItem>
                    <IconButton size="large">
                        <BookmarkIcon
                            sx={{ fontSize: { xs: "1.8rem", md: "2rem" } }}
                        />
                        <TextItem variant="h6" component="span">
                            Logout
                        </TextItem>
                    </IconButton>
                </BoxItem>
            </WrapperItem>
        </BoxContainer>
    );
}

export default LeftBar;
