import { styled } from "@mui/material/styles";
import Avatar from "@mui/material/Avatar";
import image1 from "../../image/backiee-185892.jpg";
import { useState } from "react";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import AvatarGroup from "@mui/material/AvatarGroup";
import image from "../../image/luis-villasmil-hX7_vFRFGWQ-unsplash.jpg";
import image2 from "../../image/luis-villasmil--5kerUxMu6I-unsplash.jpg";
import image3 from "../../image/luis-villasmil-mymklTVMCCo-unsplash.jpg";
import image4 from "../../image/minh-pham-5yENNRbbat4-unsplash.jpg";
import image5 from "../../image/sigmund-jzz_3jWMzHA-unsplash.jpg";
import image6 from "../../image/nicolas-horn-MTZTGvDsHFY-unsplash.jpg";

const BoxContainer = styled(Box)(({ theme }) => ({
    position: "fixed",
    ".MuiButtonBase-root": {
        padding: "0",
    },
    // top:"110",
}));

const AvatarContainer = styled(Box)(({ theme }) => ({}));

const TypographyFriends = styled(Typography)(({ theme }) => ({
    [theme.breakpoints.down("lg")]:{
        fontSize:"1.2rem"
    }
}))

const IconButtonFriends = styled(IconButton)(({ theme }) => ({
    transition: "all 0.5s",
    "&:hover":{
        transform:"scale(1.2)"
    }
}))


function OnlineFriends() {
    const [anchorEl, setAnchorEl] = useState(null);

    const isMenuOpen = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
        console.log(isMenuOpen);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };

    const menuId = "primary-search-account-menu";

    const renderMenu = (
        <Menu
            anchorEl={anchorEl}
            anchorOrigin={{
                vertical: "top",
                horizontal: "right",
            }}
            id={menuId}
            keepMounted
            transformOrigin={{
                vertical: "top",
                horizontal: "right",
            }}
            open={isMenuOpen}
            onClose={handleClose}>
            <MenuItem onClick={handleClose}>Profile</MenuItem>
            <MenuItem onClick={handleClose}>My account</MenuItem>
        </Menu>
    );

    return (
        <BoxContainer>
            <TypographyFriends variant="h5" component="div" sx={{userSelect:"none"}}>
                Online Friends
            </TypographyFriends>
            <AvatarGroup max={7} sx={{ marginTop: "1rem"  , flexWrap:"wrap"}}>
                <IconButtonFriends
                    size="large"
                    aria-label="account of current user"
                    aria-controls={menuId}
                    aria-haspopup="true"
                    onClick={handleClick}
                    >
                    <Avatar alt="Remy Sharp" src={image3}  sx={{ width: 50, height: 50 }}/>
                </IconButtonFriends>
                <IconButtonFriends
                    size="large"
                    aria-label="account of current user"
                    aria-controls={menuId}
                    aria-haspopup="true"
                    onClick={handleClick}>
                    <Avatar alt="Travis Howard" src={image} sx={{ width: 50, height: 50 }} />
                </IconButtonFriends>
                <IconButtonFriends
                    size="large"
                    aria-label="account of current user"
                    aria-controls={menuId}
                    aria-haspopup="true"
                    onClick={handleClick}>
                    <Avatar alt="Cindy Baker" src={image4}  sx={{ width: 50, height: 50 }}/>
                </IconButtonFriends>
                <IconButtonFriends
                    size="large"
                    aria-label="account of current user"
                    aria-controls={menuId}
                    aria-haspopup="true"
                    onClick={handleClick}>
                    <Avatar alt="Agnes Walker" src={image5} sx={{ width: 50, height: 50 }}/>
                </IconButtonFriends>
                <IconButtonFriends
                    size="large"
                    aria-label="account of current user"
                    aria-controls={menuId}
                    aria-haspopup="true"
                    onClick={handleClick}>
                    <Avatar alt="Trevor Henderson" src={image6} sx={{ width: 50, height: 50 }}/>
                </IconButtonFriends>

                <IconButtonFriends
                    size="large"
                    aria-label="account of current user"
                    aria-controls={menuId}
                    aria-haspopup="true"
                    onClick={handleClick}>
                    <Avatar alt="Cindy Baker" src={image2} sx={{ width: 50, height: 50 }} />
                </IconButtonFriends>

                <IconButtonFriends
                    size="large"
                    aria-label="account of current user"
                    aria-controls={menuId}
                    aria-haspopup="true"
                    onClick={handleClick}>
                    <Avatar
                        alt="Agnes Walker"
                        src="../../image/oguz-yagiz-kara-_3l3woEG6aI-unsplash.jpg"
                    />
                </IconButtonFriends>

                <IconButtonFriends
                    size="large"
                    aria-label="account of current user"
                    aria-controls={menuId}
                    aria-haspopup="true"
                    onClick={handleClick}>
                    <Avatar
                        alt="Trevor Henderson"
                        src="../../image/sigmund-jzz_3jWMzHA-unsplash.jpg"
                    />
                </IconButtonFriends>
            </AvatarGroup>
            {renderMenu}
        </BoxContainer>
    );
}

export default OnlineFriends;
