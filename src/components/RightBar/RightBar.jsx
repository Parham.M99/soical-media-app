import { Grid } from "@mui/material";
import { Box } from "@mui/system";
import { styled } from "@mui/material/styles";
import OnlineFriends from "./OnlineFriends";
import AddIcon from "@mui/icons-material/Add";
import IconButton from "@mui/material/IconButton";
import AddPostModal from "../Main/AddPostModal";

const BoxContainer = styled(Box)(({ theme }) => ({
    position: "sticky",
    top: "0",
    bottom: "0",
    marginLeft: theme.spacing(5),
    paddingLeft: theme.spacing(8),
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
    height: "100vh",
    backgroundColor: "#f5f5f5",
    display: "flex",
    flexDirection: "column",
    // justifyContent: "space-between"
    [theme.breakpoints.down("lg")]: {
        paddingLeft: theme.spacing(2),
    },
}));

const IconButtonWrapper = styled(IconButton)(({ theme }) => ({
    // position: "relative",
    // top: "45rem",
    // left: "16rem",
    // width: "70px",
    // height: "70px",
    // backgroundColor: "#40C7A6",
    // cursor: "pointer",
    // transition: "all .5s",
    // "&:hover": {
    //     backgroundColor: "#1EA3AD",
    //     transform: "scale(1.09)",
    // },
}));

function RightBar() {
    let addPostComo;

    const handleModalOpen = () => {
    };
    console.log(addPostComo);

    return (
        <>
            <BoxContainer>
                <Grid constrainer>
                    <OnlineFriends />
                </Grid>
                {/* <Grid constrainer>Galley</Grid> */}
                {/* <Grid constrainer>Categories</Grid> */}
            </BoxContainer>
        </>
    );
}

export default RightBar;
