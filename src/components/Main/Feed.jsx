import { Box } from "@mui/system";
import Post from "./Post";
import { styled, alpha } from "@mui/material/styles";

const BoxContainer = styled(Box)(({ theme }) => ({
    marginTop: theme.spacing(3),
    width: " 100%",
    marginLeft: theme.spacing(3),
    [theme.breakpoints.down("sm")]: {
        marginLeft: theme.spacing(2),
    },
}));

function Feed() {
    return (
        <BoxContainer>
            <Post
                title="Choose the perfect design"
                imgUrl="https://images.pexels.com/photos/7319337/pexels-photo-7319337.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
            />
            <Post
                title="Simply Recipes Less Stress. More Joy"
                imgUrl="https://images.pexels.com/photos/7363671/pexels-photo-7363671.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
            />
            <Post
                title="What To Do In London"
                imgUrl="https://images.pexels.com/photos/7245535/pexels-photo-7245535.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
            />
            <Post
                title="Recipes That Will Make You Crave More"
                imgUrl="https://images.pexels.com/photos/7245477/pexels-photo-7245477.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
            />
            <Post
                title="Shortcut Travel Guide to Manhattan"
                imgUrl="https://images.pexels.com/photos/7078467/pexels-photo-7078467.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
            />
            <Post
                title="Killer Actions to Boost Your Self-Confidence"
                imgUrl="https://images.pexels.com/photos/7833646/pexels-photo-7833646.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500"
            />
        </BoxContainer>
    );
}

export default Feed;
