import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";

function Post({ title, imgUrl }) {
    return (
        <>
            <Card sx={{ marginBottom: "2.5rem" }}>
                <CardActionArea>
                    <CardMedia
                        component="img"
                        height="350"
                        image={imgUrl}
                        alt="green iguana"
                    />
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                            {title}
                        </Typography>
                        <Typography variant="body2" color="text.secondary">
                            Lorem ipsum dolor sit amet, consectetur adipisicing
                            elit. Nobis voluptates officia eius laborum enim hic
                            maiores corporis, quibusdam impedit voluptas quod
                            illo, quae nihil dolorem odit deserunt, excepturi
                            perferendis optio!
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <Button size="small">Share</Button>
                    <Button size="small">Learn More</Button>
                </CardActions>
            </Card>
        </>
    );
}

export default Post;
