import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import { useState } from "react";
import { styled, alpha } from "@mui/material/styles";
import Fab from "@mui/material/Fab";
import AddIcon from "@mui/icons-material/Add";
import Tooltip from "@mui/material/Tooltip";
import TextField from "@mui/material/TextField";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import Stack from "@mui/material/Stack";



const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: "40%",
    bgcolor: "background.paper",
    boxShadow: 24,
    p: 4,
    borderRadius: 4,
};

const BoxModal = styled(Box)(({ theme }) => ({
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: "40%",
    backgroundColor: "#fff",
    boxShadow: "24",
    padding: "1.8rem 2rem",
    borderRadius: theme.shape.borderRadius,
    [theme.breakpoints.down("sm")]:{
        width: "55%",
        
    }
}));

const cancelBtnStyle = {
    color: "red",
    backgroundColor: "red",
};

const FabWrapper = styled(Fab)(({ theme }) => ({
    position: "fixed",
    bottom: 60,
    right: 50,
    backgroundColor: "#40C7A6",
    [theme.breakpoints.down("sm")]: {
        opacity: "0.7",
        bottom: 40,
        right: 2,
    },
    transition: "all ease-in-out .3s",
    "&:hover": {
        transform: "scale(1.1)",
        backgroundColor: "#1EA3AD",
        opacity: "1",
    },
}));

const currencies = [
    {
        value: "Public",
        label: "Public",
    },
    {
        value: "Hidden",
        label: "Hidden",
    },
];

function AddPostModal({ openAction }) {
    const [open, setOpen] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const [value, setValue] = useState("");

    const handleChange = (event) => {
        setValue(event.target.value);
    };

    const [currency, setCurrency] = useState("EUR");

    const handleChangeVisibility = (event) => {
        setCurrency(event.target.value);
    };
    return (
        <div>
            <Tooltip
                TransitionComponent={Fade}
                TransitionProps={{ timeout: 600 }}
                title="Add"
                onClick={handleOpen}
                // sx={IconStyle}
            >
                <FabWrapper aria-label="add">
                    <AddIcon />
                </FabWrapper>
            </Tooltip>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}>
                <Fade in={open}>
                    <BoxModal>
                        <TextField
                            fullWidth
                            id="standard-basic"
                            label="Title"
                            variant="standard"
                            margin="normal"
                        />
                        <TextField
                            id="outlined-multiline-flexible"
                            label="Multiline"
                            multiline
                            rows={4}
                            fullWidth
                            margin="normal"
                            value={value}
                            onChange={handleChange}
                        />
                        <FormControl>
                            <TextField
                                id="standard-select-currency-native"
                                select
                                margin="normal"
                                label="Visibility"
                                value={currency}
                                onChange={handleChangeVisibility}
                                SelectProps={{
                                    native: true,
                                }}
                                helperText="Please select your visibility status"
                                variant="standard">
                                {currencies.map((option) => (
                                    <option
                                        key={option.value}
                                        value={option.value}>
                                        {option.label}
                                    </option>
                                ))}
                            </TextField>
                            <FormLabel
                                component="legend"
                                sx={{ marginTop: ".7rem" }}>
                                How can Comment ?
                            </FormLabel>
                            <RadioGroup
                                aria-label="gender"
                                defaultValue="female"
                                name="radio-buttons-group">
                                <FormControlLabel
                                    value="everybody"
                                    control={<Radio />}
                                    label="Everybody"
                                />
                                <FormControlLabel
                                    value="my friends"
                                    control={<Radio />}
                                    label="My Friends"
                                />
                                <FormControlLabel
                                    value="nobody"
                                    control={<Radio />}
                                    label="Nobody"
                                />
                                <FormControlLabel
                                    value="custom (premium)"
                                    disabled
                                    control={<Radio />}
                                    label="Custom (Premium)"
                                />
                            </RadioGroup>
                        </FormControl>

                        <Stack
                            spacing={2}
                            direction="row"
                            sx={{ marginTop: "1.5rem" }}>
                            <Button variant="outlined" color="success">
                                Create
                            </Button>
                            <Button variant="outlined" color="warning">
                                Cancel
                            </Button>
                        </Stack>
                    </BoxModal>
                </Fade>
            </Modal>
        </div>
    );
}

export default AddPostModal;
